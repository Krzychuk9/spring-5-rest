package pl.kasprowski.springclient.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.server.ServerWebExchange;
import pl.kasprowski.springclient.service.ApiService;

@Controller
public class UserController {

    private ApiService apiService;

    @Autowired
    public UserController(ApiService apiService) {
        this.apiService = apiService;
    }

    @GetMapping({"", "/", "/index"})
    public String showIndexPage(){
        return "index";
    }

    @PostMapping("/users")
    public String showUserList(Model model, ServerWebExchange serverWebExchange){
        model.addAttribute("users", apiService.getUsers(serverWebExchange.
                getFormData().map(data -> new Integer(data.getFirst("limit")))));
        return "userlist";
    }
}
