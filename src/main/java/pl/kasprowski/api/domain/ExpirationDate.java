package pl.kasprowski.api.domain;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
public class ExpirationDate implements Serializable{
    private Date date;
    private int timezoneType;
    private String timezone;
}
