package pl.kasprowski.springclient.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import pl.kasprowski.api.domain.User;

import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ApiServiceIT {

    @Autowired
    private ApiService apiService;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void getUsers() throws Exception {
        List<User> users = apiService.getUsers(2);

        assertEquals(3, users.size());
    }

}